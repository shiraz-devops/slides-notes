# Shiraz-DevOps slides and required notes

You can access frontend project and deployment project in below links:

- [Frontend](https://gitlab.com/shiraz-devops/frontend)
- [Deployments](https://gitlab.com/shiraz-devops/deployments)

## Install ArgoCD

We suppose you have a k8s or at least [Minikube](https://minikube.sigs.k8s.io/docs/start/).

To install [ArgoCD](https://argo-cd.readthedocs.io/en/stable/) run these commands:

```bash
kubectl create namespace argocd
```

```bash
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Make sure all pods are in `Running` status in `argocd` namespace , then try to access argocd dashboard by running:

```bash
kubectl port-forward -n argocd svc/argocd-server 4040:443
```

You can access the dashboard in `localhost:4040`. Default username is `admin` and initial password is stored in `argocd-initial-admin-secret`.

```bash
kubectl get secrets -n argocd argocd-initial-admin-secret -o json
```

Copy `data.password` value and run this command to decode:

```bash
echo COPIED_PASSWORD | base64 -d
```

It returns the plain password and you can use it to login into argocd dashboard.
